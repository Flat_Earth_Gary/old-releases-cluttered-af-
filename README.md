# MY PUBLIC (and for the most part stable) RELEASES
This is my public repo created for releases. All releases in this repo will be stable as long as nothing else is noted.

All of these tools are completely free of charge.
Be aware that I do not take any responsibility with what you do with these tools.

Anyway, hope you enjoy the tools!
Have a great day!

# V.3 IS OUT, BIGGEST UPDATE YET

- A second version has been added within the program. The second one is more optimised to use with larger amounts of mail. Both versions are currently active. If you want to access the better, faster version go into info when running the program. If you want use the normal version (might be more stable, but is less productive when handling with larger amounts of mails) just use it as normal when calling the program.
- Fixed bunch of bugs
- Made the UI much prettier
- Made the program overall faster without losing stability


UPDATES!
- V1 First release
- V1.1 NOT STABLE SHOULD NOT BE USED
- V2 Huge update, fixed various bugs
- V2.1 Fixed security flaw

